----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 07.03.2019 13:30:29
-- Design Name:
-- Module Name: tp1_picoblaze_top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tp1_picoblaze_top is
	port (
		clk_i : in STD_LOGIC;
		reset_i : in STD_LOGIC;
		port1_i : in STD_LOGIC_VECTOR (7 downto 0);
		port1_o : out STD_LOGIC_VECTOR (7 downto 0)
	);
end tp1_picoblaze_top;

architecture Behavioral of tp1_picoblaze_top is

    -- Composant picoblaze
	component kcpsm6
		generic (
			hwbuild : std_logic_vector(7 downto 0) := X"00";
			interrupt_vector : std_logic_vector(11 downto 0) := X"3FF";
			scratch_pad_memory_size : integer := 64
		);
		port (
			address : out std_logic_vector(11 downto 0);
			instruction : in std_logic_vector(17 downto 0);
			bram_enable : out std_logic;
			in_port : in std_logic_vector(7 downto 0);
			out_port : out std_logic_vector(7 downto 0);
			port_id : out std_logic_vector(7 downto 0);
			write_strobe : out std_logic;
			k_write_strobe : out std_logic;
			read_strobe : out std_logic;
			interrupt : in std_logic;
			interrupt_ack : out std_logic;
			sleep : in std_logic;
			reset : in std_logic;
			clk : in std_logic
		);
	end component;

	-- Memory component
	component program 
		generic (
			C_FAMILY : string := "S6";
			C_RAM_SIZE_KWORDS : integer := 1;
			C_JTAG_LOADER_ENABLE : integer := 0
		);
		port (
			address : in std_logic_vector(11 downto 0);
			instruction : out std_logic_vector(17 downto 0);
			enable : in std_logic;
			rdl : out std_logic; 
			clk : in std_logic
		);
	end component;

	-- Signals for connection of KCPSM6 and Program Memory.
	--

	signal address : std_logic_vector(11 downto 0);
	signal instruction : std_logic_vector(17 downto 0);
	signal bram_enable : std_logic;
	signal in_port : std_logic_vector(7 downto 0);
	signal out_port : std_logic_vector(7 downto 0);
	signal port_id : std_logic_vector(7 downto 0);
	signal write_strobe : std_logic;
	signal k_write_strobe : std_logic;
	signal read_strobe : std_logic;
	signal interrupt : std_logic;
	signal interrupt_ack : std_logic;
	signal kcpsm6_sleep : std_logic;
	signal kcpsm6_reset : std_logic;

	--
	-- Some additional signals are required if your system also needs to reset KCPSM6.
	--

	signal cpu_reset : std_logic;
	signal rdl : std_logic;

	--
	-- When interrupt is to be used then the recommended circuit included below requires
	-- the following signal to represent the request made from your system.
	--

	signal int_request : std_logic;

begin
    -- Processeur picoblaze port map
	processor : kcpsm6
		generic map(
            hwbuild => X"00", 
            interrupt_vector => X"3FF", 
            scratch_pad_memory_size => 64)
		port map(
			address => address, 
			instruction => instruction, 
			bram_enable => bram_enable, 
			port_id => port_id, 
			write_strobe => write_strobe, 
			k_write_strobe => k_write_strobe, 
			out_port => out_port, 
			read_strobe => read_strobe, 
			in_port => in_port, 
			interrupt => interrupt, 
			interrupt_ack => interrupt_ack, 
			sleep => kcpsm6_sleep, 
			reset => reset_i, 
			clk => clk_i
		);
 
    -- M�moire programme port map
    program_rom : program --Name to match your PSM file
        generic map(
            C_FAMILY => "7S", --Family 'S6', 'V6' or '7S'
            C_RAM_SIZE_KWORDS => 2, --Program size '1', '2' or '4'
            C_JTAG_LOADER_ENABLE => 1) --Include JTAG Loader when set to '1'
        port map(
            address => address, 
            instruction => instruction, 
            enable => bram_enable, 
            rdl => kcpsm6_reset, 
            clk => clk_i
        );

    -- Multiplexeur d'entr�e
    input_mux : process (clk_i, reset_i)
    begin
        if reset_i='1' then
            in_port <= x"00";
        elsif rising_edge(clk_i) then
            case port_id is
                when x"01" => in_port <= port1_i;
                when others => in_port <= x"00";
            end case;
        end if;
    end process;
    
    -- D�multiplexeur de sortie
    output_demux : process (clk_i, reset_i)
    begin
        if reset_i = '1' then
            port1_o <= x"00";
        elsif rising_edge(clk_i) then
            if write_strobe = '1' then
            
                case port_id is
                    when x"01" => 
                        port1_o <= out_port;
                        
                    when others => 
                        port1_o <= x"00";
                end case;
            end if;
        end if;
    end process;
    
    kcpsm6_sleep <= '0';
    interrupt <= '0'; 
    
end Behavioral;