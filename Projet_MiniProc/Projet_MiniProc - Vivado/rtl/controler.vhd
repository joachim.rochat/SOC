----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    10:56:31 05/02/2018
-- Design Name:
-- Module Name:    controler - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity controler is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;

		flag_i   : in std_logic;
		opcode_i : in std_logic_vector(2 downto 0);

		rwmem_o : out std_logic;

		selaa_o : out std_logic;
		rwreg_o : out std_logic;
		opalu_o : out std_logic_vector(1 downto 0);
		inalu_o : out std_logic;
		inreg_o : out std_logic;
		oealu_o : out std_logic;
		ldres_o : out std_logic;
		ldpc_o  : out std_logic;
		selpc_o : out std_logic;
		ldir_o  : out std_logic
	);
end controler;

architecture Behavioral of controler is

	--Type
	type ctrl_state is(
		FETCH, DECOD, EXEC, MEM_ACCESS, WRITE_BACK
	);

	--Signal
	signal next_state_s, current_state_s : ctrl_state;

begin

	-- State Machine (3 process description)

	-- Next state process
	next_state_process : process(current_state_s)
	begin
		-- Cycle through standard execution cycle
		case current_state_s is
			when FETCH =>
				next_state_s <= DECOD;
			when DECOD =>
				next_state_s <= EXEC;
			when EXEC =>
				next_state_s <= MEM_ACCESS;
			when MEM_ACCESS =>
				next_state_s <= WRITE_BACK;
			when WRITE_BACK =>
				next_state_s <= FETCH;
		end case;
	end process;

	-- Current state process
	current_state_process : process(clk_i, reset_i)
	begin
		if (reset_i = '1') then
			current_state_s <= FETCH;
		elsif rising_edge(clk_i) then
			current_state_s <= next_state_s;
		end if;
	end process;

	-- Output state process
	output_state_process : process(current_state_s)
	begin
		case current_state_s is

			-- FSM fetch state
			when FETCH =>
				-- Default values
				rwmem_o <= cREAD;				-- Memory in read mode
				selaa_o <= '0';					-- A is 10..9
				rwreg_o <= cREAD;				-- Register in read mode
				opalu_o <= cSUB_OP;			-- Use sub ALU operation
				inalu_o <= '1';					-- Sub 0x00 to first operand
				inreg_o <= '0';					-- Data comes from ALU
				oealu_o <= '0';					-- Do not write to DM bus
				ldres_o <= '0';					-- Do not propagate ALU output / flag
				ldpc_o  <= '0';					-- Do not load PC
				selpc_o <= '0';					-- Use '1' as PC increment
				ldir_o  <= '1';					-- Load IR with next instr

				-- Change ctrl output depending on current opcode
				case opcode_i is
					when cLOAD_INSTR =>
						null;
					when cSTORE_INSTR =>
						null;
					when cMOVE_INSTR =>
						null;
					when cSUB_INSTR =>
						null;
					when cINC_INSTR =>
						null;
					when cCMP_INSTR =>
						null;
					when cBR_INSTR =>
						null;
					when cJMP_INSTR =>
						null;
					when others =>
						null;
				end case;

			-- FSM decode state
			when DECOD =>
				-- Default values
				rwmem_o <= cREAD;					-- Memory in read mode
				rwreg_o <= cREAD;					-- Registers in read mode
				ldres_o <= '0';						-- Do not propagate ALU output / flag
				ldpc_o  <= '0';						-- Do not load PC
				ldir_o  <= '0';						-- Do not load IR

				-- Change ctrl output depending on current opcode
				case opcode_i is
					when cLOAD_INSTR =>
						inreg_o <= '1';				-- Data comes from DM
					when cSTORE_INSTR =>
						opalu_o <= cSUB_OP;		-- Use sub ALU operation
						inalu_o <= '0';				-- Sub 0x00 to first operand
						selaa_o <= '1';				-- A is 12..11
					when cMOVE_INSTR =>
						opalu_o <= cSUB_OP;		-- Use sub ALU operation
						inalu_o <= '0';				-- Sub 0x00 to first operand
					when cSUB_INSTR =>
						opalu_o <= cSUB_OP;		-- Use sub ALU operation
					when cINC_INSTR =>
						opalu_o <= cINC_OP;		-- Use inc ALU operation
						inalu_o <= '0';				-- Sub 0x00 to first operand
						selaa_o <= '1';				-- A is 12..11
					when cCMP_INSTR =>
						opalu_o <= cCMP_OP;		-- Use cmp ALU operation
					when cBR_INSTR =>
						null;
					when cJMP_INSTR =>
						null;
					when others =>
						null;
				end case;

			-- FSM execute state
			when EXEC =>
				-- Default values
				rwmem_o <= cREAD;						-- Memory in read mode
				rwreg_o <= cREAD; 					-- Registers in read mode
				ldres_o <= '1';							-- Propagate ALU output / flag
				ldpc_o  <= '1';							-- Load PC
				ldir_o  <= '0';							-- Do not load IR

				-- Change ctrl output depending on current opcode
				case opcode_i is
					when cLOAD_INSTR =>
						null;
					when cSTORE_INSTR =>
						null;
					when cMOVE_INSTR =>
						null;
					when cSUB_INSTR =>
						null;
					when cINC_INSTR =>
						null;
					when cCMP_INSTR =>
						null;
					when cBR_INSTR =>
						-- If flag = 1, jump with offset
						if flag_i = '1' then
							selpc_o <= '1';
						else
							selpc_o <= '0';
						end if;
					when cJMP_INSTR =>
						selpc_o <= '1';					-- Jump with offset
					when others =>
						null;
				end case;

			-- FSM memory access state
			when MEM_ACCESS =>
				-- Default values
				rwmem_o <= cREAD;						-- Memory in read mode
				rwreg_o <= cREAD;						-- Registers in read mode
				ldres_o <= '0';							-- Do not propagate ALU output / flag
				ldpc_o  <= '0';							-- Do not load PC
				ldir_o  <= '0';							-- Do not load IR

				-- Change ctrl output depending on current opcode
				case opcode_i is
					when cLOAD_INSTR =>
						null;
					when cSTORE_INSTR =>
						oealu_o <= '1';					-- Propagate ALU output to registers
						rwmem_o <= cWRITE;			-- Memory in write mode
					when cMOVE_INSTR =>
						null;
					when cSUB_INSTR =>
						null;
					when cINC_INSTR =>
						null;
					when cCMP_INSTR =>
						null;
					when cBR_INSTR =>
						null;
					when cJMP_INSTR =>
						null;
					when others =>
						null;
				end case;

			-- FMS writeback state
			when WRITE_BACK =>
				-- Default values
				rwmem_o <= cREAD;						-- Memory in read mode
				rwreg_o <= cREAD;						-- Registers in read mode
				ldres_o <= '0';							-- Do not propagate ALU output / flag
				ldpc_o  <= '0';							-- Do not load PC
				ldir_o  <= '0';							-- Do not load IR

				-- Change ctrl output depending on current opcode
				case opcode_i is
					when cLOAD_INSTR =>
						rwreg_o <= cWRITE;			-- Registers in write mode
					when cSTORE_INSTR =>
						oealu_o <= '0';					-- Do not write to DM bus
					when cMOVE_INSTR =>
						rwreg_o <= cWRITE;			-- Registers in write mode
					when cSUB_INSTR =>
						rwreg_o <= cWRITE;			-- Registers in write mode
					when cINC_INSTR =>
						rwreg_o <= cWRITE;			-- Registers in write mode
					when cCMP_INSTR =>
						null;
					when cBR_INSTR =>
						null;
					when cJMP_INSTR =>
						null;
					when others =>
						null;
				end case;

			-- FSM invalid state
			when others =>
				null;

		end case;
	end process;

end Behavioral;
