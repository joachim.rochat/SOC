----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    16:09:36 05/02/2018
-- Design Name:
-- Module Name:    alu - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity alu is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;

		inalu_i  : in std_logic;
		opalu_i  : in std_logic_vector(1 downto 0);
		ldres_i  : in std_logic;

		a_i      : in std_logic_vector(7 downto 0);
		b_i      : in std_logic_vector(7 downto 0);

		carry_o  : out std_logic;
		flag_o   : out std_logic;
		result_o : out std_logic_vector(7 downto 0)
	);
end alu;

architecture Behavioral of alu is

	--Signals
	signal b_s : std_logic_vector(7 downto 0);
	signal alu_out_s : std_logic_vector(7 downto 0);
	signal flag_s : std_logic;

begin

	--Code

	-- Use B or 0x0 as ALU 2nd input
	b_s <= b_i when inalu_i = '1' else
		(others => '0') when inalu_i = '0';

	-- ALU main block
	process(a_i, b_s, opalu_i)
	begin
		case opalu_i is
			-- Substract operation
			when cSUB_OP =>
				alu_out_s <= std_logic_vector(signed(a_i) - signed(b_s));

			-- Compare operation
			when cCMP_OP =>
				if a_i = b_i then
					flag_s <= '1';
				else
					flag_s <= '0';
				end if;

			-- Increment operation
			when cINC_OP =>
				alu_out_s <= std_logic_vector(unsigned(a_i) + 1);

			-- Other operations
			when others =>
				alu_out_s <= (others => 'X');
		end case;
	end process;

	-- Handle output / flag propagation and reset
	process(clk_i, reset_i)
	begin
		if reset_i = '1' then
			result_o <= (others => '0');
		elsif rising_edge(clk_i) then
			if ldres_i = '1' then
				flag_o <= flag_s;
				result_o <= alu_out_s;
			end if;
		end if;
	end process;

end Behavioral;
