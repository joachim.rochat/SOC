----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    14:33:54 11/02/2015
-- Design Name:
-- Module Name:    prog_mem - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity prog_mem is
	port (
		clk_i : in std_logic;
		adr_i : in std_logic_vector(10 downto 0);
		data_io : inout std_logic_vector(15 downto 0);
		ce_i : in std_logic;
		rw_i : in std_logic
	);
end prog_mem;

architecture Behavioral of prog_mem is

	type prog_mem_type is array(1023 downto 0) of std_logic_vector(15 downto 0);
	--program memory
	signal prog_mem_s : prog_mem_type := ( --    short program:
		0  => "0000000000000000", --       LOAD  R0,M[0]
		1  => "0000100000000001", --       LOAD  R1,M[1]
		2  => "0111000010000000", --       SUB R2,R0,R1
		3  => "1001000000000000", --       INC R2
		4  => "0101100000000000", --       CPY R3,R0
		5  => "0011100000000010", --       STORE R3,M[2]
		6  => "1000100000000000", --LOOP1: INC R1
		7  => "1010000010000000", --       CMP R0,R1
		8  => "1100000000000010", --       BR END
		9  => "1110111111111101", --			 JMP LOOP1
		10 => "1110000000000000", --END:   JMP END
		others => "0000000000000000"
	);
	signal adrint : std_logic_vector(4 downto 0);
	signal outint : std_logic_vector(15 downto 0);
	signal ceint :  std_logic;

begin

	adrint <= adr_i(4 downto 0);
	ceint <= '1' when ((ce_i='1') and (31>=to_integer(unsigned(adr_i)))) else '0';

	process (clk_i)
	begin
		if falling_edge(clk_i) then
			outint <= prog_mem_s(to_integer(unsigned(adrint)));
			if rw_i=cWRITE then
				if ceint='1' then
					prog_mem_s(to_integer(unsigned(adrint))) <= data_io;
				end if;
			end if;
		end if;
	end process;

	data_io <=  (others => 'Z') when (ceint='1' and rw_i=cWRITE) else outint;

end Behavioral;
