----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    15:59:40 05/02/2018
-- Design Name:
-- Module Name:    pc - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity pc is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;

		ldpc_i   : in std_logic;
		selpc_i  : in std_logic;

		jmp_i : in std_logic_vector(11 downto 0);
		pc_o : out std_logic_vector(10 downto 0)
	);
end pc;

architecture Behavioral of pc is

	--Signals
	signal pc_s : integer := 0;

begin

	--Code

	-- Increment or add offset to PC
	process(clk_i, reset_i)
	begin
		if reset_i = '1' then
			pc_s <= 0;
		elsif rising_edge(clk_i) then
			if ldpc_i = '1' then
				-- Increment PC
				if selpc_i = '0' then
					pc_s <= pc_s + 1;
				-- Add offset to PC
				elsif selpc_i = '1' then
					pc_s <= pc_s + to_integer(signed(jmp_i));
				end if;
			end if;
		end if;
		pc_o <= std_logic_vector(to_signed(pc_s,11));
	end process;
end Behavioral;
