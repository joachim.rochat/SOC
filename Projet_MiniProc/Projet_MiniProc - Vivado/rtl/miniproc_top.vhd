----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:19:13 05/02/2018 
-- Design Name: 
-- Module Name:    miniproc_top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity miniproc_top is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		addr_bus_p_o : out std_logic_vector(10 downto 0);
		data_bus_p_o : out std_logic_vector(15 downto 0);
		
		addr_bus_d_o  : out std_logic_vector(10 downto 0);
		data_bus_d_io : inout std_logic_vector(7 downto 0);
		
		rw_o   : out std_logic
	);
end miniproc_top;

architecture Behavioral of miniproc_top is

	--Signals
	signal addr_bus_d_s,addr_bus_p_s : std_logic_vector(10 downto 0);
	signal data_bus_p_s : std_logic_vector(15 downto 0);
	signal rw_s : std_logic;
	
begin

	--Buses and signals connections
	addr_bus_p_o <= addr_bus_p_s;
	data_bus_p_o <= data_bus_p_s;
	
	addr_bus_d_o <= addr_bus_d_s;
	
	rw_o   <= rw_s;

	--Components
	cmp_proc : proc 
	port map (
		clk_i   => clk_i,
		reset_i => reset_i,
		
		addr_bus_p_o => addr_bus_p_s,
		data_bus_p_i => data_bus_p_s,
		
		addr_bus_d_o  => addr_bus_d_s,
		data_bus_d_io => data_bus_d_io,
		
		rwmem_o  => rw_s
	);
		
	cmp_data_mem : data_mem
	port map (
		clk_i => clk_i,
		adr_i => addr_bus_d_s,
		data_io => data_bus_d_io,
		ce_i => '1',
		rw_i => rw_s
	);
	
	cmp_prog_mem : prog_mem
	port map (
		clk_i => clk_i,
		adr_i => addr_bus_p_s,
		data_io => data_bus_p_s,
		ce_i => '1',
		rw_i => '1'
	);
	
end Behavioral;


