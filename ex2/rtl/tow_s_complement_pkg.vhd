--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes,
--		 constants, and functions
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package tow_s_complement_pkg is

  -- Composant des registres 8 bits
  component register_8bits is
  	port (
  		clk_i   : in std_logic;
  		reset_i : in std_logic;

  		load_i : in std_logic;
  		port_i : in std_logic_vector (7 downto 0);
  		port_o : out std_logic_vector (7 downto 0)
  	);
  end component;

  -- Composant de la FSM
  component statemachine is
  	port (
  		clk_i   : in std_logic;
  		reset_i : in std_logic;

  		start_i : in std_logic;
  		port_i  : in std_logic_vector (7 downto 0);

  		rdy_o      : out std_logic;
  		load_in_o  : out std_logic;
  		load_out_o : out std_logic;

  		port_o  : out std_logic_vector(7 downto 0)
  	);
  end component;

  -- Fonction de complément à 2
  function complement2 (signal vect_in_s : in std_logic_vector) return std_logic_vector;

end tow_s_complement_pkg;

package body tow_s_complement_pkg is

  -- Corps de la fonction de complément à 2
  function complement2 (signal vect_in_s : in std_logic_vector) return std_logic_vector is
    variable vect_out_v : std_logic_vector(7 DOWNTO 0) := x"00";
  begin
    vect_out_v := std_logic_vector(unsigned(not vect_in_s) + 1);
    return vect_out_v;
  end function;

end tow_s_complement_pkg;
