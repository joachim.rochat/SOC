----------------------------------------------------------------------------------
-- Company: 	HEIA-FR
-- Engineer: 	Joachim Rochat
--
-- Create Date:    16:36:11 02/25/2016
-- Design Name:
-- Module Name:    statemachine - Behavioral
-- Project Name: 		Exercice 1
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity statemachine is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;

		start_i : in std_logic;
		port_i  : in std_logic_vector (7 downto 0);

		rdy_o      : out std_logic;
		load_in_o  : out std_logic;
		load_out_o : out std_logic;

		port_o  : out std_logic_vector(7 downto 0)
	);
end statemachine;

architecture Behavioral of statemachine is

	-- Type
	type FSM_states is
	(
		IDLE, READ_IN, WRITE_OUT
	);

	-- Signal
	signal next_state, current_state : FSM_states;

begin

	--State machine - next state
	reg_next : process(clk_i, reset_i)
	begin
		if reset_i = '1' then
			current_state <= IDLE;
		elsif rising_edge(clk_i) then
			case current_state is
				when IDLE =>
					if start_i = '1' then
						next_state <= READ_IN;
					else
						next_state <= IDLE;
					end if;
				when READ_IN =>
					next_state <= WRITE_OUT;
				when WRITE_OUT =>
					next_state <= IDLE;
			end case;
			current_state <= next_state;
		end if;
	end process;

	--Output of state machine
	reg_out : process(current_state)
	begin
		case current_state is
			when IDLE =>
				rdy_o 			<= '1';
				load_in_o 	<= '0';
				load_out_o 	<= '0';
			when READ_IN =>
				rdy_o 			<= '0';
				load_in_o 	<= '1';
				load_out_o 	<= '0';
			when WRITE_OUT =>
				rdy_o 			<= '0';
				load_in_o 	<= '0';
				load_out_o 	<= '1';
		end case;
	end process;

	--Combinatory 2's complement (combinatory)
	port_o <= std_logic_vector(unsigned(not port_i) + 1);

end Behavioral;
